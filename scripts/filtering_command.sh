
#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for filtering.x

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR
source ${SCRIPT_PATH}/aux/arg_parser_filtering.sh

# check input files
tb_folder=${INPUT}
[ -d "${tb_folder}" ] || die "Traceback directory not found, missing input files, '$tb_folder'"

# check number of files
num_files=`ls ${tb_folder}/sequence-labels_*.bin | wc -l`
[ $num_files -ge 1 ] || die "No files matching sequence-labels_*.bin format in '$tb_folder'" 
echo "Found $num_files files to process"	

# output dir
filter_folder=${OUTDIR}
mkdir -p ${filter_folder} || die "Error creating filterustering directory '$filter_folder'"

# run command over all files in num_files
for ((i=1;i<=num_files;++i)); do
   ${BIN_PATH}/filtering.x ${tb_folder}/sequence-labels_${i}.bin -o ${filter_folder}/filtered-labels_${i}.txt
done

# for small datasets we merge the output in one file
for ((i=1;i<=num_files;++i)); do
   cat ${filter_folder}/filtered-labels_${i}.txt
done >  ${filter_folder}/filtered-labels_ALL.txt
 

# for ((i=1;i<=num_files;++i)); do
#    rm ${filter_folder}/filtered-labels_${i}.txt
# done