#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: script to generate final MCs' SEEDS

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $TSV_FASTA 
source ${SCRIPT_PATH}/aux/arg_parser_seeds.sh
 
# check variables
tab_fasta=${TSV_FASTA}
filtered_labels=${INPUT}

[ -s "$tab_fasta" ] || die "TSV fasta file missing or empty '$tab_fasta'"
[ -s "$filtered_labels" ] || die "TSV fasta file missing or empty '$filtered_labels'"
mkdir -p ${OUTDIR} || die "Error creating output directory '${OUTDIR}'"

# add sequences and protein names to the filtered file
awk 'NR==FNR{seq[$1]=$3}NR!=FNR{print $0, substr(seq[$1],$2,$3-$2+1)}' ${tab_fasta} ${filtered_labels} | \
awk 'length($5)<$3-$2+1 {print $1, $2, $3-1, $4, $5} length($5)==$3-$2+1 {print $0}' > ${OUTDIR}/tt
awk 'NR==FNR{name[$1]=$2}NR!=FNR{print $1, name[$1], $2, $3, $4, $5}' ${tab_fasta}  ${OUTDIR}/tt  >  ${OUTDIR}/filtered-labels_complete.txt 

rm  ${OUTDIR}/tt

# split in metaclusters seed fasta files
# make seed subfolder:
seeds_dir=${OUTDIR}/seeds
mkdir -p ${seeds_dir} || die "Error creating seeds directory '${seeds_dir}'"

# list of MCs:
awk '{print $5}'  ${OUTDIR}/filtered-labels_complete.txt | awk '!seen[$1]++' | sort -g >  ${OUTDIR}/MClist.txt                                
list=`cat ${OUTDIR}/MClist.txt`

# create MCXXX.fasta seed files
for mc in $list; 
do 
    awk -v mc=$mc '$5==mc{print ">" $2 "/" $3 "-" $4; print $6}'  ${OUTDIR}/filtered-labels_complete.txt \
    > ${seeds_dir}/MC${mc}.fasta; 
    [ -s "${seeds_dir}/MC${mc}.fasta" ] || die "Error building seed, 'MC${mc}.fasta'" && echo "MC${mc}.fasta - ok"
done  

# compute relevant quantities, such as: size, average length, std-avlen
# initiate with  header to MCstatistics.txt
echo "#MC N_sequences average_length std_deviation_AL"  >  ${OUTDIR}/MCstatistics.txt
# compute statistics
for mc in $list; 
do 
    awk -v mc=$mc '$5==mc{size++; avlen+=$4-$3+1;  avlen2+=($4-$3+1)^2;}END{print mc, size, avlen/size, sqrt((avlen2/size) - (avlen/size)^2)}' \
    ${OUTDIR}/filtered-labels_complete.txt; 
done >>  ${OUTDIR}/MCstatistics.txt