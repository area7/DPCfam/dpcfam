#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for distance computing

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check input files
prycl_folder=${INPUT}

[ -d "$prycl_folder" ] || die "Primary cluster folder not found, '$prycl_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); 
do
    prycl=${prycl_folder}"/"${i}".bin"
    [ -s "$prycl" ] || die "Missing primary cluster binary file, '$prycl'"
done

# output dir
dmat_folder=${OUTDIR}
mkdir -p ${dmat_folder} || die "Error creating distance matrix directory '${dmat_folder}'"

# Generate matrix by cycling over matrix NUM_BLOCKSXNUM_BLOCKS (note: it is symmetric so we will only build hal of it)

# first, generate diagonal elements:
for ((i=1;i<=NUM_BLOCKS;++i)); do
	${BIN_PATH}/dist_Icl_D.x ${prycl_folder}/${i}.bin ${prycl_folder}/${i}.bin -o ${dmat_folder}/${i}-${i}.bin   
done
  
# then, generate off-diagonal elements:
for ((i=1;i<=NUM_BLOCKS;++i)); do
	for ((j=i+1;j<=NUM_BLOCKS;++j)); do	
		${BIN_PATH}/dist_Icl_ND.x ${prycl_folder}/${i}.bin ${prycl_folder}/${j}.bin -o ${dmat_folder}/${i}-${j}.bin   
	done
done