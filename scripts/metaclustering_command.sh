
#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for metaclustering code

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check input files
dmat_folder=${INPUT}
input_files=""

[ -d "$dmat_folder" ] || die "Invalid input! Primary cluster distance matrix folder not found, '$dmat_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); do
	for ((j=i;j<=NUM_BLOCKS;++j)); do	
      dmat_block=${dmat_folder}"/"${i}-${j}".bin"
      [ -s "$dmat_block" ] || die "Missing distance matrix block file, '$dmat_block'"
      input_files+="$dmat_block "
   done
done

# output dir
metacl_folder=${OUTDIR}
mkdir -p ${metacl_folder} || die "Error creating metaclustering directory '$metacl_folder'"

# run command
${BIN_PATH}/metaclustering.x ${input_files} -o ${metacl_folder}/MCoutput.txt




