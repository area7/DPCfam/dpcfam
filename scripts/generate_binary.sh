#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for input2binary.x to convert cleaned primary clustering output into binary

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check input files
prycl_folder=${INPUT}

[ -d "$prycl_folder" ] || die "Primary cluster folder not found, '$prycl_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); 
do
    prycl=${prycl_folder}"/C"${i}".cl"
    [ -s "$prycl" ] || die "Missing primary cluster clean file, '$prycl'"
done

# outputs
mkdir -p ${OUTDIR} || die "Error creating output directory '${OUTDIR}'"

# cycle over NUM_BLOCKS 
for ((i=1;i<=NUM_BLOCKS;++i)); do
   ${BIN_PATH}/input2binary.x ${prycl_folder}/C${i}.cl -o ${OUTDIR}/${i}.bin
done