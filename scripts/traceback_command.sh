#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for traceback code

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $PC_LABELS, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser_traceback.sh

# check input files
prycl_folder=${INPUT}
input_files=""

[ -d "$prycl_folder" ] || die "Invalid input! Primary cluster folder not found, '$prycl_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); 
do
    prycl=${prycl_folder}"/"${i}".bin"
    [ -s "$prycl" ] || die "Missing primary cluster binary file, '$prycl'"
    input_files+="$prycl "
done

[ -s "$PC_LABELS" ] || die "Missing primary clusters labels file, '$PC_LABELS'"

# output dir
tb_folder=${OUTDIR}
mkdir -p ${tb_folder} || die "Error creating traceback directory '$tb_folder'"


# run command
${BIN_PATH}/traceback.x ${input_files}  -l ${PC_LABELS}  -o ${tb_folder}/
