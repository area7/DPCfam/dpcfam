#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: wrapper for primary clustering code

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check input files
align_folder=${INPUT}
[ -d "$align_folder" ] || die "Alignments folder not found, '$align_folder'"
for ((i=1;i<NUM_BLOCKS;++i)); 
do
    align=${align_folder}"/"${i}".blasted"
    [ -s "$align" ] || die "Missing alignment file, '$align'"
done

# output dir
prycl_folder=${OUTDIR}
mkdir -p ${prycl_folder} || die "Error creating primary clusters directory '$prycl_folder'"

# cycle over NUM_BLOCKS 
for ((i=1;i<=NUM_BLOCKS;++i)); do
	# check is there is already an ouput file, if yes delete it since c++ program will append
	if [ -f "${prycl_folder}/${i}.cl" ] 
	then 
		rm ${prycl_folder}/${i}.cl
		warn "Found old version of file ${prycl_folder}/${i}.cl. Old file removed!"
	fi

	${BIN_PATH}/primaryclustering.x ${align_folder}/${i}.blasted ${prycl_folder}/${i}.cl 0.2 0.5  >> ${prycl_folder}/Icl.log
done

 