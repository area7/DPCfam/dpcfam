#!/bin/bash

# check from where you are running script
[[ ${PWD##*/} == scripts ]] && cd ..

[[ $(type -t die) == function ]] || source ./scripts/aux/error_handle.sh

# check if env. variables are set correctly (manually or config file)
bad=0
for var in NUM_BLOCKS DATA_FOLDER INPUT_FASTA;
do
	if [ -z "${!var}" ] ;
	then
		warn "$var empty or not set!";
		let bad++;
	fi
done

if [ $bad -gt 0 ];
then
   die "Missing input variables. Source configuration file or manually set the environment variables"
fi

[ -s $INPUT_FASTA ] || die "Fasta file missing or empty '$INPUT_FASTA'. Check configuration file"

[[ $NUM_BLOCKS =~ ^[0-9]+$ ]] || die "NUM_BLOCKS invalid value '$NUM_BLOCKS'. Check configuration file" # is integer
[ $NUM_BLOCKS -ge 1 ] || die "NUM_BLOCKS should be greater or equal that one. Check configuration file" 

mkdir -p $DATA_FOLDER || die "Error creating data directory '$DATA_FOLDER'. Check configuration file"
DATA_FOLDER=$(cd ${DATA_FOLDER}; pwd) # replace relative with absolute path