#!/bin/bash

function die()
{
	echo >&2 "*** $0: $*, abort.";
	exit 1
}

function warn()
{
	echo >&2 "*** $0: $*"
}