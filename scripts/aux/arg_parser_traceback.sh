#!/bin/bash

# include error handling functions
[[ $(type -t die) == function ]] || source ./error_handling.sh

function Usage()
{
	cat <<-__EOF__>&2

		Usage: $0 [-f] -n BLOCKS -l LABELS [-o OUTPUT] INPUT

		    INPUT       input directory

		    -o OUTPUT   output directory
		    -n BLOCKS   number of blocks
            -l LABELS   primary clusters labels

        Description:
            Assigns a label to each domain belonging to a primary cluster (PC).
            The label is simply the metacluster label of the PC.
	__EOF__

	exit 1
}

if [ $# -eq 0 ]
then
	Usage
fi

bad=0
# force=false;
INPUT=""
OUTDIR="${PWD}"
NUM_BLOCKS=1
PC_LABELS=""
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help|"")	    Usage ;;
		# -f|--force)       force=true ; shift ;;
		-o|--output)        OUTDIR="$2" ; shift 2 ;;         
        -l|--labels)        PC_LABELS="$2" ; shift 2 ;;
		-n|--num-blocks)
			NUM_BLOCKS=$2
            [[ $NUM_BLOCKS =~ ^[0-9]+$ ]] || die "[-n NUMBER OF BLOCKS] invalid value '$NUM_BLOCKS'" # is integer
            [ $NUM_BLOCKS -ge 1 ] || die "[-n NUMBER OF BLOCKS] should be greater or equal than one" 
			shift 2
			;;
		*) INPUT="$1" ; shift ;;
            
	esac
done

if [ -z "${INPUT}" ]; then
    warn "Missing INPUT argument."
    Usage
fi