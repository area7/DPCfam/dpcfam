#!/bin/bash

# include error handling functions
[[ $(type -t die) == function ]] || source ./error_handling.sh

function Usage()
{
	cat <<-__EOF__>&2

		Usage: $0 -o OUTPUT INPUT

		    INPUT       input directory containing MC domain lists - 'sequence-labels_*.bin'

		    -o OUTPUT   output directory

        Description:
            Removes single domain occurrences within each MC domain list given as input.
            For those domains remaining it chooses a single representative
            for every group of overlaping domains.

	__EOF__

	exit 1
}

if [ $# -eq 0 ]
then
	Usage
fi

bad=0
# force=false;
INPUT=""
OUTDIR="${PWD}"
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help|"")	    Usage ;;
		-o|--output)        OUTDIR="$2" ; shift 2;;         
		*) INPUT="$1" ; shift ;;
            
	esac
done

if [ -z "${INPUT}" ]; then
    warn "Missing INPUT argument."
    Usage
fi