#!/bin/bash

# include error handling functions
[[ $(type -t die) == function ]] || source ./error_handling.sh

function Usage()
{
	cat <<-__EOF__>&2

		Usage: $0 [-f] [-o OUTPUT] -t TSV-FASTA INPUT

		    INPUT           list of domains and its corresponding MC labels (output from filtering step)

		    -o OUTPUT       output directory
		    -t TSV-FASTA    tab separated fasta     

        Description:
            Generates a fasta file per metacluster by splitting the input file.
            Additionally compute basic statistics of the metaclusters.
	__EOF__

	exit 1
}

if [ $# -eq 0 ]
then
	Usage
fi

bad=0
# force=false;
INPUT=""
OUTDIR="${PWD}"
TSV_FASTA=""
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help|"")	    Usage ;;
		# -f|--force)       force=true ; shift ;;
		-o|--output)        OUTDIR="$2" ; shift 2 ;;         
        -t|--tsv-fasta)     TSV_FASTA="$2" ; shift 2 ;;
		*) INPUT="$1" ; shift ;;
            
	esac
done

if [ -z "${INPUT}" ]; then
    warn "Missing INPUT argument."
    Usage
fi