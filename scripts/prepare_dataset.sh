#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: script to prepare input data for DPCfam clustering

# export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh
 
# check variables
[ -s "$INPUT" ] || die "Fasta file missing or empty '$INPUT'"
mkdir -p ${OUTDIR} || die "Error creating data directory '${OUTDIR}'"


# Build dataset
fastas_dir=${OUTDIR}"/fastas"
mkdir -p ${fastas_dir} || die "Error creating fastas directory '$fastas_dir'"

fasta_name="$(basename -- $INPUT)"
numeric_fasta=${OUTDIR}"/"${fasta_name}".numid"
tab_fasta=${OUTDIR}"/"${fasta_name}".tab"

# generate tab separated fasta
awk '$1~">"{printf "\n%s ",$1} $1!~">" {printf "%s",$1}'  ${INPUT} | awk 'NR>1{print substr($0,2)}' > ${OUTDIR}/tmp
shuf --random-source=${INPUT} ${OUTDIR}/tmp | awk '{print NR,$0}' > ${tab_fasta}
rm ${OUTDIR}/tmp


# compute total number of sequences:
tot_lines=$(wc -l < ${tab_fasta})
htot=$((${tot_lines}/2)) 

[ $htot -ne 0 ] || die "Error creating tab separated fasta file. Check input file"

[ $htot -gt $NUM_BLOCKS ] || die "NUM_BLOCKS should be at least smaller than $htot. Check you configure file" 

# Split in blocks
echo "Splitting input into $NUM_BLOCKS blocks..."

# shuffling needed for dist_Icl to work properly
shuf --random-source=$tab_fasta $tab_fasta > tab_fasta_shuf 
num_lines=$(( (${tot_lines}+${NUM_BLOCKS}-1)/${NUM_BLOCKS}))
split -l ${num_lines} -d -a 4 tab_fasta_shuf ${fastas_dir}"/tmp."

# convert files into fastas:
for ((i=1;i<=NUM_BLOCKS;++i)); do
	i_str=$(printf "%04d" $(($i-1)) )
	awk '{printf ">%s\n%s\n", $1,$3}' ${fastas_dir}/tmp.${i_str} > ${fastas_dir}/$i.fasta
	rm ${fastas_dir}/tmp.${i_str}
done

rm tab_fasta_shuf
wc -l ${fastas_dir}/*.fasta