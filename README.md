[![License](https://img.shields.io/badge/License-BSD%202--Clause-orange.svg)](https://opensource.org/licenses/BSD-2-Clause)
[![pipeline status](https://gitlab.com/area7/DPCfam/dpcfam/badges/master/pipeline.svg)](https://gitlab.com/area7/DPCfam/dpcfam/commits/master)
[![Platform](https://img.shields.io/badge/platform-Linux-blue.svg)](https://www.linux.org/)


# DPCfam

DPCfam is a two-step clustering algorithm which classifies homologous protein regions into protein families. It was originally developed to classify all proteins in UniRef50 (v. 201707). The resulting dataset is available at https://dpcfam.areasciencepark.it.

<!-- Originally developed to classify UniRef50  -->

This repository provides an implementation of the DPCfam pipeline as described in [DPCfam original paper](https://doi.org/10.1186/S12859-021-04013-X).
The core of the algorithm is written in C++. Bash is used as the scripting language connecting the steps of the pipeline.

The code was developed and tested on Centos but should run on any linux-based systems.

DPCfam uses the [moodycamel::ConcurrentQueue library](https://github.com/cameron314/concurrentqueue) freely available provided citation (Simplified BSD license). 

## Publications

Barone, F., Russo, E. T., Villegas Garcia, E. N., Punta, M., Cozzini, S., Ansuini, A., & Cazzaniga, A. (2023). Protein family annotation for the Unified Human Gastrointestinal Proteome by DPCfam clustering. bioRxiv. https://doi.org/10.1101/2023.04.21.537802 

Russo, E. T., Barone, F., Bateman, A., Cozzini, S., Punta, M., & Laio, A. (2022). DPCfam: Unsupervised protein family classification by Density Peak Clustering of large sequence datasets. PLOS Computational Biology, 18(10), e1010610. https://doi.org/10.1371/journal.pcbi.1010610

Russo, E. T., Laio, A., & Punta, M. (2021). Density Peak clustering of protein sequences associated to a Pfam clan reveals clear similarities and interesting differences with respect to manual family annotation. BMC bioinformatics, 22(1), 1-28. https://doi.org/10.1186/s12859-021-04013-x



## Requirements
External softwares
- blastp (ncbi-blast-2.2.30+), installed and linked

Some standard gnu programs are required:
- g++ (>9.4.0)
- make
- sort
- shuf 
- awk

## Installation
On a linux-based system execute the following commands:
```
$ git clone https://gitlab.com/area7/DPCfam/dpcfam.git
$ cd dpcfam
$ make
```
Executables are built inside the `./bin` folder

## Usage

DPCfam pipeline takes as input a fasta file containing the list of proteins you want to classify. The output is a set of metaclusters (MCs), stored in `/path/to/output/metaclusters/seeds`, each represented by a fasta file, `MCXXX.fasta`, containing its corresponding protein domains.

In its current configuration the DPCfam pipeline performs an all vs all protein alignment and clustering.

The following steps are required in order to run the pipeline

0) Check that `blastp (ncbi-blast-2.2.30+)` is installed and available
    ```
    $ blastp -version
    ```

1) Open file **configure** (root dir) and specify the following variables:
    ```
    NUM_BLOCKS = split dataset in N blocks (smaller N requires more RAM!)
    DATA_FOLDER = directory where dpcfam will store its output
    INPUT_FASTA = path to fasta file with proteins to analyse
    ```
    When specifying the paths do *not* include the trailing slash (/)!

2) In the package root directory, run:
    ```
    $ ./run_all.sh
    ```

    This will run the full pipeline.


## Pipeline file description

As described in the previous section, the DPCfam pipeline takes as input a set of protein sequences in fasta format and outputs a set of clusters, i.e. our putative protein families, each stored as a separate fasta file containing its sequence members.
```
>PROTEIN_ID1/start-end
FKAAGLAVDTTYDGVFVRSIVENSKAAGVLQVGDVITEVDGKPVAKSDDLIQYINSTKKVGDEVDVKFTRTNKDKQEQKEAKIPLMDLHRV
>PROTEIN_ID2/start-end
IPDLYITKPGPVADAMEMVSVADHTSKSDSEILVTTVKREQGTVFKLIRALIHPYQNLTKESQNYEAVKQDSRDVQRAFMANSKQTAVMEAHRLAEKEKELDFVGVRVDVQN
...
```

In the current version, almost all intermediate pipeline files are stored in `tsv` format with custom columns. Next version of the code will update files to binary format. The table below describes the contents of each file.

| Folder | File | Description | Columns | format |
| ---  | --- | --- | --- | --- |
| primary_clustering| X.cl | Blast alignments | queryID - alnID - centerID - center - distCenter - searchID -  sStart - sEnd -  qStart - qEnd - score - evalue - iscenter - rho - delta | tsv |
| primary_clustering| CX.clust | Reduced version of blast alignmeents | queryID - centerID - searchID - sStart sEnd | tsv |
| primary_clustering| X.bin | Binary version of the previous file (use ./bin/read_input.x to read it)   | queryID - centerID - searchID - sStart sEnd | binary |
| distance_matrix | X-Y.bin | Distance matrix between primary clusters block X and Y (use ./bin/read_output.x to read it) | pcID1 - pcID2 - distance | binary |
| metaclusters | MCoutput.txt | Primary cluster labels | pcID - density - delta - mcID | tsv |
| metaclusters | peaksIdx.txt | List of primary clusters which are density peaks | pcID  | tsv |
| metaclusters | filtered-labels_ALL.txt | Protein labels (index is the row on the input fasta file) | proteinID - start - end - mcID | tsv |
| metaclusters/seeds | MCXXX.fasta | Fasta file containing the sequences of the metacluster MCXXX | proteinID/start-end | fasta |


**NOTE:** Intermediate files are subject to change in future versions of the pipeline and are not intended to be used as standard output from the pipeline. However, information about the contents of these files is useful for in-depth analysis of the clustering process. We will try to keep track of their changes in the provided table.


<!-- ### Global testing
In the package root directory, run:

`./run_all.sh test`

This will run the full pipeline using a small test dataset. The operation should take few minutes. At the end a series of ouputs will be checked with reference data.
 -->
## Example data
In the folder `data_example` we included two datasets to test the pipeline.

The first example, `small_dataset.fasta`, is a toy dataset with 200 proteins and serves the purpose of quickly testing the pipeline.

The second example, `big_dataset.fasta`, includes about 5,000 protein sequences containing 3 different Pfam families, reduced at 50 P.I. using cd-hit. The following section explains in more detail its content.

### Description of proteins in big_dataset.fasta

Proteins in this set contain either a PFAM Lon_C family (PF05362), a Lon_substr_bdg family (PF02190) or a  Pyrophosphatase family (PF00719).

Poteins have been selected by downloading Pfam's alignments of each family above (Uniprot alignment).
We then downloaded each protein listed in the alignment from the UniprotKB website, specifically:
- 62066 proteins containing a PF05362 family region
- 51692 proteins containing a PF02190 family region
- 27691 proteins containing a PF00719 family region

After running cd-hit at 50 P.I, 5,077 protein sequences remained.

Note that PF05362 and PF02190 appear often in the same architecture.
Proteins may contain many other families.
*Downloaded on the 16th of march 2021*
*Pfam v 33.1*
*UniproKB v 2021_01*

### Estimated runtime of the pipeline using big_dataset.fasta: 

On Intel i7, 8th gen and using *5* blocks:
- **Step 1**: alignment generation with blast - **75 minutes**
- **Step 2**: primary clustering - **10 minutes**
- **Step 3**: computation of distance matrix between primary clusters - **5 minutes**
- **Step 4**: metaclustering + merging - **<1 minute**
- **Last steps**: traceback, filtering, generation of MCs - **<1 minute**

## Scaling up

DPCfam pipeline consists on a series of steps (see previous section), each of which can be run independent from each other. This modular approach allows the algorithm to tackle bigger datasets if needed.

If wanted to use with larger datasets, aside from choosing an appropiate number of blocks, the main bottleneck of the pipeline is the alignment generation using `blastp` (you can check how to run it sequentially in `./scripts/blast_command.sh`). The good news is that this step is embarrassingly parallel, meaning you can perform the alignment of each block of proteins independent from the others. 

Aside from this the other critical points are *Step 3: distance matrix* and *Step 4: metaclustering + merging*. The former is already implemented in parallel and can be sped up simply by specifying more threads (check `./bin/dist_Icl.x -n #threads`) while the latter is not computationally demanding but can use quite a bit of RAM depending on your dataset.
Just to give an estimate, while running *DPCfam-Step-4* on UniRef50 we used around 700 GB of RAM.

<!-- ## Citing this work
If you use the code or data in this package, please cite: -->






## Authors
Algorithm and code by Elena Tea Russo & Federico Barone under the supervision of Alessandro Laio (SISSA, Trieste, IT), Marco Punta (HSR, Milan, IT) and Stefano Cozzini (AREA SCIENCE PARK, Trieste, IT).

Developed in SISSA, Trieste, IT (2016-2020) and AREA SCIENCE PARK, Trieste, IT (2021-)
