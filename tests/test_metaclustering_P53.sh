#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of metaclustering with P53 datased

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
BIN_PATH=${TEST_PATH%/*}/bin
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH} && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# no parser. Hardcoded values
LOGFILE=P53_metacl.log
NUM_BLOCKS=3

source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/setP53_metacl
OUT_PATH=test${DATASET}_logs/setP53_metaclustering


echo ">>> Dataset P53: Testing comput_dmat.sh" | tee -a $LOGFILE

mkdir -p ${OUT_PATH} &>> $LOGFILE

# run command
${BIN_PATH}/metaclustering.x ${REF_PATH}/data/P53dmat/P53dmat_??.bin -o ${OUT_PATH}/MCoutput.txt >> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

# remove unused files
rm ${OUT_PATH}/peaksIdx.txt

# compare results to reference
EC=1

# compare labels
cmp ${OUT_PATH}/MCoutput.txt ${REF_PATH}/reference/MCoutput_ref.txt &> ${OUT_PATH}/tt
if [ $? -eq 0 ]; then
	passed MCoutput.txt $LOGFILE
	rm -rf ${OUT_PATH}/ &>> $LOGFILE
	EC=$((EC - 1))
else
	failed MCoutput.txt $LOGFILE
	cat ${OUT_PATH}/tt &>> $LOGFILE
fi

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi