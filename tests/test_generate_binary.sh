#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of ./generate_binary.sh 

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# parser -> $LOGIFLE, $DATASET, $NUM_BLOCKS 
source ${TEST_PATH}/aux/arg_parser.sh
source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/set${DATASET}
OUT_PATH=test${DATASET}_logs/primary_clusters

echo ">>> Dataset ${DATASET}: Testing generate_binary.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/generate_binary.sh -o ${OUT_PATH} -n ${NUM_BLOCKS} ${REF_PATH}/primary_clusters &>> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

EC=$NUM_BLOCKS
# check blocks outputs (i.bin)
for ((i=1;i<=NUM_BLOCKS;++i)); do
	cmp ${OUT_PATH}/${i}.bin ${REF_PATH}/primary_clusters/${i}.bin &> ${OUT_PATH}/tt
	if [ $? -eq 0 ]; then
		passed ${i}.bin $LOGFILE
		rm ${OUT_PATH}/${i}.bin
		EC=$((EC - 1))
	else
		failed ${i}.bin $LOGFILE
		cat ${OUT_PATH}/tt >> $LOGFILE
	fi
done

# tmp file
rm ${OUT_PATH}/tt

# remove if empty
[ "$(ls -A ${OUT_PATH})" ] || rm -r ${OUT_PATH}


# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi