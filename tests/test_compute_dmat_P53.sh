#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of distance matrix using P53 dataset 

# TODO: reformat to respect set[A-D] standard

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
BIN_PATH=${TEST_PATH%/*}/bin
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH} && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# no parser. Hardcoded values
# source ${SCRIPT_PATH}/aux/arg_parser.sh
LOGFILE=P53_distIcl.log
NUM_BLOCKS=3

source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/setP53_distIcl
OUT_PATH=testP53_logs/setP53_distance_matrix

echo ">>> Dataset P53: Testing comput_dmat.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/compute_dmat.sh -o ${OUT_PATH} -n $NUM_BLOCKS ${REF_PATH}/primary_clusters &>> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

# check output: this time we compare it in txt format
# then, generate off-diagonal elements:
for ((i=1;i<=NUM_BLOCKS;++i)); do
	for ((j=i;j<=NUM_BLOCKS;++j)); do	
		${BIN_PATH}/read_output.x ${OUT_PATH}/${i}-${j}.bin > ${OUT_PATH}/${i}-${j}.txt 2>> $LOGFILE
	done
done
cat ${OUT_PATH}/?-?.txt > ${OUT_PATH}/P53_distIcl.txt 
sort -n -s -k2,2 ${OUT_PATH}/P53_distIcl.txt | sort -n -s -k1,1 > ${OUT_PATH}/P53_distIcl_sorted.txt

EC=1
cmp ${OUT_PATH}/P53_distIcl_sorted.txt ${REF_PATH}/reference/P53dmat_ref.txt &> ${OUT_PATH}/tt
if [ $? -eq 0 ]; then
	passed P53_distIcl_sorted.txt $LOGFILE
	rm -rf ${OUT_PATH}/
	EC=$((EC - 1))
else
	failed P53_distIcl_sorted.txt $LOGFILE
	cat ${OUT_PATH}/tt >> $LOGFILE
fi

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A testP53_logs)" ] || rm -r testP53_logs
	echo "   Test passed!"
fi
