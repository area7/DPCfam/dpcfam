#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of ./filtering_command.sh

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# parser -> $LOGIFLE, $DATASET, $NUM_BLOCKS 
source ${TEST_PATH}/aux/arg_parser.sh
source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/set${DATASET}
OUT_PATH=test${DATASET}_logs/metaclusters

echo ">>> Dataset ${DATASET}: Testing filtering_command.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/filtering_command.sh -o ${OUT_PATH} ${REF_PATH}/metaclusters &>> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

# check outputs
shopt -s extglob;
num_files=`ls ${REF_PATH}/metaclusters/filtered-labels_+([0-9]).txt 2>> ${LOGFILE} | wc -l`

{ [[ $num_files =~ ^[0-9]+$ ]] && [ $num_files -ge 1 ] ;} || \
die "Missing filtered-labels_*.txt . Please check '${LOGFILE}'"

EC=$num_files
for ((i=1;i<=num_files;++i)); do 
	cmp ${OUT_PATH}/filtered-labels_${i}.txt ${REF_PATH}/metaclusters/filtered-labels_${i}.txt &> ${OUT_PATH}/tt
	if [ $? -eq 0 ]; then
		passed filtered-labels_${i}.txt $LOGFILE
		rm ${OUT_PATH}/filtered-labels_${i}.txt
		EC=$((EC - 1))
	else
		failed filtered-labels_${i}.txt $LOGFILE
		cat ${OUT_PATH}/tt &>> $LOGFILE
	fi
done

# tmp file
rm ${OUT_PATH}/tt

# remove if empty
[ "$(ls -A ${OUT_PATH})" ] || rm -r ${OUT_PATH}

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi