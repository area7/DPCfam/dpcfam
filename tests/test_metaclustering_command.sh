#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of ./metaclustering_command.sh

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# parser -> $LOGIFLE, $DATASET, $NUM_BLOCKS 
source ${TEST_PATH}/aux/arg_parser.sh
source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/set${DATASET}
OUT_PATH=test${DATASET}_logs/metaclusters

echo ">>> Dataset ${DATASET}: Testing metaclustering_command.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/metaclustering_command.sh -o ${OUT_PATH} -n $NUM_BLOCKS ${REF_PATH}/distance_matrix &>> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

# check outputs in out_list
out_list=( MCoutput.txt peaksIdx.txt )
{ [[ ${#out_list[@]} =~ ^[0-9]+$ ]] && [ ${#out_list[@]} -ge 1 ] ;} || \
die "Reference files not found. Please check '${LOGFILE}'"

EC=${#out_list[@]}
for file in ${out_list[@]}; do
	cmp ${OUT_PATH}/$file ${REF_PATH}/metaclusters/$file &> ${OUT_PATH}/tt
	if [ $? -eq 0 ]; then
		passed $file $LOGFILE
		rm ${OUT_PATH}/${file}
		EC=$((EC - 1))
	else
		failed $file $LOGFILE
		cat ${OUT_PATH}/tt >> $LOGFILE
 	fi
done

# tmp file
rm ${OUT_PATH}/tt

# remove if empty
[ "$(ls -A ${OUT_PATH})" ] || rm -r ${OUT_PATH}

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi