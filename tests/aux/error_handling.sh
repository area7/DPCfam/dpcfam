#!/bin/bash

function die()
{
	echo -e "*** $0: $*, abort." | tee -a $2
	exit 1
}

function warn()
{
	echo -e "*** $0: $*" | tee -a $2
}
