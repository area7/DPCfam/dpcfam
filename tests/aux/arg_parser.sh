#!/bin/bash

# include error handling functions
[[ $(type -t die) == function ]] || ./error_handling.sh

function Usage()
{
	cat <<-__EOF__>&2

		Usage: $0 -d DATASET -n BLOCKS -l LOGFILE

		    -d DATASET   label of the dataset - [A,B,C,D]
		    -n BLOCKS    number of blocks (default = 1)
		    -l LOGFILE   full path to logfile

	__EOF__

	exit 1
}

if [ $# -eq 0 ]
then
	Usage
fi

# force=false;
LOGFILE=""
DATASET=""
NUM_BLOCKS=1
while [ $# -gt 0 ]
do
	case $1 in
		-l|--logfile)        LOGFILE="$2" ; shift 2;;
        -d|--dataset)        DATASET="$2" ; shift 2;;         
		-n|--num-blocks)
			NUM_BLOCKS=$2
            [[ $NUM_BLOCKS =~ ^[0-9]+$ ]] || die "[-n NUMBER OF BLOCKS] invalid value '$NUM_BLOCKS'" # is integer
            [ $NUM_BLOCKS -ge 1 ] || die "[-n NUMBER OF BLOCKS] should be greater or equal than one" 
			shift 2
			;;
		*) die "Invalid argument $1" ;;
            
	esac
done

[ -z "${LOGFILE}" ] && die "Invalid -l LOGFILE value";
[ -z "${DATASET}" ] && die "Invalid -d DATASET value";