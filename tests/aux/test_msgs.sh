#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDC="\e[0m"

function failed()
{
	echo -e "   ${RED}(x) test:${ENDC} error on $1" | tee -a $2
}

function passed()
{
	echo -e "   ${GREEN}(v) test:${ENDC} $1 ok" | tee -a $2
}
