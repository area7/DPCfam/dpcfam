
// Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfam pipeline
// SPDX-License-Identifier:  BSD-2-Clause
// Description: c++ program to filter relevant sequences of metaclusters after traceback



/*******************************************************************************
* Traceback the MC labels from the primary clusters to its correspondent protein domain
* 
* Input format [binary]: cID, cSize, searchID, searchStart, searchEnd.
* MC labels format [txt]: MC_labels
* Output format [binary]: sequenceLabel struct - searchID, search start, search end, MC label.;
* 
******************************************************************************/


#include <algorithm>
#include <cstring> // memcpy
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <math.h>  
#include <sstream>  
#include <random>
#include <unistd.h> // getopt
#include <unordered_map> 
#include <vector>
 
#include <dpcfam/datatypes.h>
#include <dpcfam/normalization.h>

/*******************************************************************************
* Loads binary file into std::vector<T>
*
* @param filename full path to file
* @param labels map that stores the label for each primary cluster
******************************************************************************/
int load_labels(std::string filename, std::unordered_map<int32_t, int64_t> & labels)
{
    std::ifstream inFile (filename);
    std::string line;
	
    int32_t cID;
    int32_t density;
    double minDistance;
    int64_t label;
    

	// test file open   
	if (inFile) 
	{        
	    // read the elements in the file into an unordered map
	    while(std::getline(inFile, line))
	    {
            // Create a stringstream of the current line
            std::stringstream ss(line);
            ss >> cID >> density >> minDistance >> label;
            labels[cID] = label;
	    }
	}
    else 
    {
        throw std::system_error(errno, std::system_category(), "failed to open "+ filename);
    }

   
    inFile.close();

    return 0;
}

/*******************************************************************************
* Assigns a metacluster label to each alignment (search sequence) inside a primary cluster
*
* @param clusterAlign pointer to a primary cluster alignment (search sequence)
* @param labels map that stores the labels for each primary cluster
******************************************************************************/
std::vector<SequenceLabel> assign_search_label(SmallCA * clusterAlign,  std::unordered_map<int32_t, int64_t> &labels, const uint64_t length)
{
    // Note that as implemented, all values have a label even if they are not present in the dmatII
    auto end = clusterAlign + length;
    auto low = clusterAlign;

    std::vector<SequenceLabel> searchLabel;
    searchLabel.reserve(length);

    while (low != end)
    {
        uint32_t cID = low->qID;

        // find last occurrence
        auto high = std::upper_bound(low, end, cID, compare_qID());

        // compute the difference
        auto count = high - low;

        // add cluster size to the list
        for (auto p = low; p < high; ++p)
        {
            // skip non assigned cIDs
            auto itLabel = labels.find(cID);
            if (itLabel == labels.end() || itLabel->second < 0) {continue;}

            searchLabel.emplace_back(SequenceLabel(p->sID, p->sstart, p->send, labels[cID])    );
        }

        // move to next element in vector (not immediate next)
        low = low + count;
    }


    return searchLabel;
}


int main(int argc, char *argv[])
{

	SmallCA * bufferCA;

    // label vector: cluster label for each node
    std::unordered_map<int32_t, int64_t> labels;
    std::vector<SequenceLabel> searchLabel;

    //-------------------------------------------------------------------------
    // Argument parser

    int opt;
    std::string outputPath{"./"}; 
    std::string labelFilename{""}; 
    std::vector<std::string> filenames;
    int num_output_files = 0;

    while ((opt = getopt(argc, argv, "l:n:ho:")) != -1) 
    {
        switch (opt) 
        {
        case 'n':
            num_output_files = std::atoi(optarg);
            break;
        case 'l':
            labelFilename = optarg;
            break;
        case 'o':
            outputPath = optarg;
            // TODO: check for missing /
            break;
        case 'h':
            // go to default

        default: /* '?' */
            std::cerr << "Usage: \n";
            std::cerr << "\t " << argv[0] << " INPUT1 INPUT2 ... -l MC_LABELS [-o OUTPUT_PATH] [-n NUM_OUTPUT] \n\n";
            std::cerr << "\t INPUT              input filenames \n\n";
            std::cerr << "\t -l MC_LABELS       file containing a MC label for each primary cluster. \n\n";
            std::cerr << "\t -o OUTPUT_PATH     output path. \n\n";
            std::cerr << "\t -n NUM_OUTPUT      **if possible** output will be divided into NUM_OUTPUT files. \n\n";

            std::cerr << "Description:\n\t" << argv[0] << " assigns an MC label to each domain inside a primary cluster. \n\n";
            std::cerr << "\t It takes as input the content of all primary clusters (same input as dist_Icl) plus a file \n containing the labels for each primary cluster.\n";
            std::cerr << "\t It outputs all domains with its corresponding MC label. \n";
            std::cerr << "\t NOTE: the program will try to divide the output into NUM_OUTPUT files conditioned by not spliting \n an MC label in different files.\n\n";
            std::cerr << "\t Input format [binary]: queryID+center, qSize, searchID, searchStart, searchEnd.\n";
            std::cerr << "\t MC labels format [txt]: MC_labels.\n";
            std::cerr << "\t Output format [binary]: sequenceLabel struct - searchID, search start, search end, MC label. \n\n";
            exit(EXIT_FAILURE);
        }
    }


    // input checks
    if (labelFilename == "")
    {
        std::cerr << "Error: missing MC labels file (-l MC_LABELS)" << "\n";
        exit(EXIT_FAILURE);
    }

    if (optind >= argc) {
        std::cerr << "Error: missing input file." << "\n";    
        exit(EXIT_FAILURE);
        
    }

    // TODO: check outpath exists


    if (num_output_files > 100)
    {
        std::cerr << "Error: maximum number of output files is 100." << "\n";    
        exit(EXIT_FAILURE); 
    }


    while(optind < argc)
    {
        filenames.push_back(argv[optind]);
        std::cerr << "Input " << filenames.size() << " = " << argv[optind] << "\n";
        ++optind;
    }

    // if num_output_files not defined, default to filenames.size()
    if (num_output_files == 0)
    {
        num_output_files = filenames.size();
    }

    std::cerr << "Total input files: " << filenames.size() << "\n";    
    
    //-------------------------------------------------------------------------


    std::vector<SmallCA*> indexes; 
    std::vector<std::ifstream> files; 
    std::vector<uint64_t> fileLines; 
    uint64_t bytes{0};
    uint64_t lines{0};
    uint64_t totalLines{0};
    
    // read all files sizes to allocate total memory
    for (const auto & filename: filenames)
    {
        std::ifstream file (filename, std::ifstream::binary);
        if(file.is_open())
        {
            file.seekg (0, file.end);
            bytes = file.tellg();
            lines = bytes/sizeof(SmallCA);
            fileLines.push_back(lines);
            totalLines += lines;
            file.seekg (0, file.beg);
            files.push_back(std::move(file));
        }
        else{
            std::cerr << "Error: file " << filename << " missing or corrupted.\n";
            exit(EXIT_FAILURE);
        }
    }
    
    // allocate total buffer
    try { bufferCA = new SmallCA [totalLines];
    }
    // Catch Block handle error
    catch (const std::bad_alloc& e) {
    std::cerr << "Memory Allocation failed. Try reducing the number of files to process! \n"
             << e.what()
             << std::endl;
             throw;
    }
    // populate buffer with each file
    auto fileBuffer = bufferCA;
    for (uint64_t i = 0; i < files.size(); ++i)
    {
        // std::cout << "indexes: " << indexes.size() << std::endl;
        files[i].read ((char*) fileBuffer, sizeof(SmallCA)*fileLines[i]);
        if (!files[i])
        {
            std::cout << "Error reading file " << filenames[i] << ": only " << files[i].gcount() << " could be read.";
        }
        files[i].close();
        fileBuffer += fileLines[i];
    }

    // load_labels
    load_labels(labelFilename, labels);


    // sort wrt qID+center
    if (    !std::is_sorted( bufferCA, bufferCA+totalLines, compare_qID() )    )
    {   
        radix_sort((unsigned char*) bufferCA, totalLines, 16, 4, 0);
    }

    if ( !std::is_sorted(bufferCA, bufferCA+totalLines, compare_qID()) )
    {
        throw std::runtime_error("Radix sort failed! Check if cIDs are 32 bit unsigned integers.");
    }


    // assign search sequence label
    searchLabel = assign_search_label(bufferCA, labels, totalLines);
    
    delete [] bufferCA;

    // sort wrt to sID
    // TODO: implement radix_sort for std::vector
    radix_sort((unsigned char*) searchLabel.data(), searchLabel.size(), 12, 4, 0);

    // sort wrt label
    // TODO: implement radix_sort for std::vector
    radix_sort((unsigned char*) searchLabel.data(), searchLabel.size(), 12, 4, 8);


    // split output into different files ordered by sID and metaclusters
    size_t avgLines = size_t(searchLabel.size() / num_output_files);
    
    auto localStartIt = searchLabel.begin();
    auto localEndIt = localStartIt;


    for (int i = 1; i <= num_output_files; ++i)
    {
        if (    localEndIt == searchLabel.end()     )
        {
            num_output_files = i-1;
            break;
        }
    	std::string outFilename = outputPath + "sequence-labels_" + std::to_string(i) + ".bin";
    	// std::cout << "outFilename: " << outFilename << '\n';
    	std::ofstream outFile(outFilename, std::ofstream::binary);
		

        if ( (searchLabel.end() - (localStartIt + avgLines)) <= 0   )
        {
            localEndIt = searchLabel.end();
        }
        else
        {
            localEndIt = std::upper_bound(localStartIt, searchLabel.end(), (localStartIt + avgLines)->label, compare_label());
            if ( searchLabel.end() - localEndIt <= avgLines*.25)
            {
                localEndIt = searchLabel.end();
            }
        }

    
		outFile.write((char*)&(*localStartIt), (localEndIt - localStartIt) * sizeof(SequenceLabel));
		localStartIt = localEndIt;
    	
    	outFile.close();
    }


    std::cerr << "Total output files: " << num_output_files << "\n";    

    return 0;

}
