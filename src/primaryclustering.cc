
// Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfam pipeline
// SPDX-License-Identifier:  BSD-2-Clause
// Description: C++ program to perform primary clustering of local pairwise alignments on a single query.
// USAGE:  ./a.out  input_FOLDER  output_FOLDER dpar gpar
// input is a BLAST output obtained from:
// blastp  -query $myquery -db $mydb -evalue 0.1 -max_target_seqs 200000 -out $myout -outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue score"



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <stdexcept>

#include <dpcfam/datatypes.h>

using namespace std;


//---------------------------------------------------------------------------------------------------------
// SEGMENTS DISTANCE on the QUERY
//--------------------------------------------------------------------------------------------------------- 
double dist(Segment i, Segment j){
	int hi, lo;
	double inte, uni;
	int istart, iend, jstart, jend;
    istart= i.qstart; iend= i.qend; jstart=j.qstart; jend=j.qend;
    //calculate intersection
    inte=0;
	hi=iend;
    if(jend<hi) hi=jend;
    lo=istart;
    if(jstart>lo) lo=jstart;
    if(hi>lo) inte=hi-lo;
    //calculate union
    hi=iend;
    if(jend>hi) hi=jend;
    lo=istart;
    if(jstart<lo) lo=jstart;
    uni=hi-lo;
return (uni-inte)/uni;
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
/////////////////////////////////   M   A   I   N    //////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------





int main(int argc, char** argv) {

	// TODO: add checks on argc
	(void)argc;
	
	ifstream infile; // input 
	ofstream outfile; // output

	double dpar; // parameter for the density estimation kernerl
	double gpar; // parameter for gap search
	dpar=atof(argv[3]);
	gpar=atof(argv[4]);
	
	int nalldata;
	vector<Segment> allSegments;

    // filenames
	string infile_path, outfile_path;
	stringstream sstm;
	// input file
	sstm << argv[1];
	infile_path = sstm.str();
	// outputf ile
	sstm.str("");
	sstm << argv[2];
	outfile_path = sstm.str();


	cout << "\n \n    Input File:        " << infile_path << endl;
	cout << "\n \n    Output File:        " << outfile_path << endl;






	infile.open (infile_path.c_str());
	if (!infile.is_open()) { cout << "INFILE" << infile_path << " NOT GOOD" << endl; return 1; }
	//cout << "\n \n    Input File:        " << infile_path  << endl;
 
 	
 
    // ++++++++++++++++++++++++++++++++++ READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
    string line; 
	while( getline(infile, line)) {
		int qseqid, sseqid;  // id numerico
		int qstart, qend, sstart, send, length;
		double evalue;
		int score;
		// data that I am not currently using.
    	int  qlen, slen;
		double pident;
    	
		istringstream iss(line); // make fileline into stream
		// leggi dallo stream (controllando IDTYPE)
		iss >> qseqid >> sseqid >> qstart >> qend >> sstart >> send >> qlen >> slen >> length >> pident >> evalue >> score;
		//generate a temporary Segment that i will then append to the Segments vector	
		Segment temp;
 		temp.qid=qseqid;
		temp.sid=sseqid;
		temp.qstart=qstart;
		temp.qend=qend+1;
		temp.sstart=sstart;
		temp.send=send+1;
		temp.len=length;
		temp.score=score;
		temp.evalue=evalue;
		temp.rho=0;
		temp.delta=0;
		temp.isc=0;
		
		allSegments.push_back(temp);
		
	}
	
	infile.close();
    
	nalldata=allSegments.size();
    
 	// ++++++++++++++++++++++++++++++++++ END READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	    
	//cout << "*** Segmenti TOTALI letti : " << allSegments.size() << endl;

	//cout << "*** Prima QID:" << allSegments[0].qid << endl;
	//cout << "*** Ultima QID:" << allSegments[allSegments.size()-1].qid << endl;


    // ---  EXTRACT QUERY-SPECIFIC ALIGNMENTS (NOTE THAT DATA ARE ALREASY SORTED BY QUERY, THUS IT IS EASY)

	int thisquery, begin, end;
	begin=0; end=-1;
	
	//cout<< " [[[ Iniziamo: " << end << " <? " << nalldata << " ]]] " << endl;

	while ( end < nalldata-1 ) {

	//select the alignments 
	begin=end+1;
        thisquery=allSegments[begin].qid;
	//cout << "This query is query " << thisquery << endl;
	//cout << "Begin is " << begin << " and End is (was) " << end << endl;
	for(int i=begin; i<nalldata; ++i){
		//cout<< i << " " << allSegments[i].qid << endl;
		if(allSegments[i].qid != thisquery) {end=i-1; break;}
		if(i==nalldata-1){end=nalldata-1;}
	}


	// copy the selected package of alignments with the same query in the new vector
	vector<Segment> mySegments;
	copy(allSegments.begin()+begin, allSegments.begin()+end+1, back_inserter(mySegments)); 

	//cout << "----- BEGIN " << begin << " END " << end << endl;
	cout << "Segmenti locali letti : " << mySegments.size() << endl;
	//cout << "QID:" << thisquery << endl;
	//cout << "Prima QID:" << mySegments[0].qid << endl;
	//cout << "Ultima QID:" << mySegments[mySegments.size()-1].qid << endl;


	// DO NOT ANALYISE QUERIES WITH LESS THAN 20 ALIGNMENTS
	if(mySegments.size()>20){



//////////////////////////////////////////////////////// FROM HERE
	
	cout << " ...     ANALIZZO  ......................................." << endl;



    vector<int> centers;
    int ndata;
    int n_putative_centers;


    // CLEANING SUPERPOSITIONS ON THE SAME ALIGNMENTS
   
   	vector<int> todel;    
	for(size_t i=0; i<mySegments.size();++i) {
	    for(size_t j=i+1; j<mySegments.size();++j) {
	    	if(mySegments[i].sid == mySegments[j].sid){
	    			if( dist( mySegments[i], mySegments[j]) < dpar ) {
	    			  //check if it is alterdy there
	    			  int repetita=0;
	    			  int indextodel;
	    			  if(mySegments[i].score > mySegments[j].score) { indextodel=j; }
	    			  else { indextodel=i; }
	    			  for(size_t k=0; k<todel.size(); ++k) {if(todel[k] == indextodel) repetita=1;}
	    			  if(repetita==0){
	    					todel.push_back(indextodel);
					}
				}
	    	}
	    }
	}

     
    //cout << "   TOO MUCH SELF-OVERLAP ON :" << todel.size() << " ELEMENTS\n   REMOVING: ";
    //for(int td=0; td<todel.size(); ++td) { cout << todel[td] << " "; }
    //cout << endl;
	for(int td=todel.size()-1; td>=0; --td ){  mySegments.erase (mySegments.begin()+td);  }
	cout << "- Segmenti effettivi : " << mySegments.size() << endl;   
    //if (mySegments.size()<20) return 1;


    ndata =  mySegments.size();

    // DEFINE NUMBER OF PUTATIVE CENTRES IN FUNCTION OF THE NUMBER OF DATA, IF MORE THAN 200 DATA NPC IS 20, OTHERWISE IT IS NDATA/200
    if (ndata>200) n_putative_centers=20;
    else n_putative_centers=ndata/10;
    if(n_putative_centers<2) n_putative_centers=2;
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE RHO
    
    //density inizialization
    for(size_t i=0;i<mySegments.size();++i) { 
        int seed;
        seed=mySegments[i].sid+mySegments[i].qstart+mySegments[i].qend-mySegments[i].sstart*mySegments[i].send;
        srand(seed); 
        mySegments[i].rho = 1 + 0.1*((double) rand() / (RAND_MAX));
        //cout << i << " " << seed << " " << mySegments[i].rho<<endl;
    }   

    for(size_t i=0; i<mySegments.size();++i) { 
     	for(size_t j=i+1; j<mySegments.size();++j){
    		double d;
   			d = dist( mySegments[i], mySegments[j]);// + 0.00001*((double) rand() / (RAND_MAX));
   			// stepwise kernel (large as dpar)
   			if(d<dpar) { mySegments[i].rho += 1; mySegments[j].rho += 1;}
    	}
    }

   

    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE DELTA
        
    for(size_t i=0; i<mySegments.size();++i) {
        //set the highest value of delta, this will be the value of the higher density peak
    	mySegments[i].delta=1000;
    	for(size_t j=0; j<mySegments.size();++j){
    		if(i==j) continue;
    		if(mySegments[j].rho <= mySegments[i].rho) continue;
    		double d;
    		d = dist( mySegments[i], mySegments[j]) + 0.00001*((double) rand() / (RAND_MAX));
    		if( d < mySegments[i].delta ) mySegments[i].delta=d; 
    		
    	}
    	
    }
    
   
   
   
   
    // -----------------------------------------------------------------------------------------------------------------------    
    //  SEARCH FOR THE FIRST n_putative_centers "PUTATITE CENTRES", i.e. those with higher gamma=delta*rho
   for(int npc=0; npc<n_putative_centers; ++npc){
	
		double maxgamma=0;
		int maxi=-1;
		for(size_t i=0; i<mySegments.size();++i) {
		 double gamma; 
		 gamma = mySegments[i].rho * mySegments[i].delta;
		 if( (gamma > maxgamma) && mySegments[i].isc !=1 ) {maxgamma = gamma; maxi=i;}
		}
		mySegments[maxi].isc=1;
		//put the index of the center in a specific vector
		centers.push_back(maxi);
		
    }

    // -----------------------------------------------------------------------------------------------------------------------    
    //  FIND "TRUE" CENTER USING THE GAP CRITERION, i.e. serch for the last "gap" between points (wrt gamma) such that -log(gamma2/gamma1) > gpar 
    
    int lastgap=0;
	for(size_t c=0; c<centers.size()-1; ++c){
			double gamma1 = mySegments[ centers[c] ].rho * mySegments[ centers[c] ].delta;
			double gamma2 = mySegments[ centers[c+1] ].rho * mySegments[ centers[c+1] ].delta;
			if( -log(gamma2/gamma1) > gpar ) lastgap=c;
			//cout << c << " " << centers[c] << " " << lastgap << " gamma1 " << gamma1 << " gamma2 " << gamma2 << " LOGgamma1 " << log(gamma1) << " LOGgamma2 " << log(gamma2) << "gamma1 " << gamma1 << " log(gamma2/gamma1) " << log(gamma2/gamma1) << endl;
	
	}    
	 
	
	// delete all centers after the gap
    for(size_t c=lastgap+1; c<centers.size(); ++c){
  	  //cout<< "cleaning " << centers[c] << endl; 
	  mySegments[centers[c]].isc=0;
    }
    centers.erase(centers.begin()+lastgap+1, centers.begin()+centers.size());
  	
  	
	//cout << " lastgap " << lastgap << endl;
	//for(int c=0; c<centers.size(); ++c) cout << centers[c] << " " ;
  	
  	
   
   

    // -----------------------------------------------------------------------------------------------------------------------    
    //  CLUSTERING
    
    outfile.open (outfile_path.c_str(),ios::app); // OPENING FINAL FILE
	if (!outfile.is_open()) { cout << "OUTFILE " << outfile_path << " NOT GOOD" << endl; return 1; }
    
    int incl=0;
	int centers_size_int = static_cast<int>(centers.size());
	int mySegments_size = static_cast<int>(mySegments.size());
	if (centers.size() != static_cast<std::vector<int>::size_type>(centers_size_int)){
		throw std::runtime_error(std::string("Centers.size() cannot be cast to 'int'"));
	}
	if (mySegments.size() != static_cast<std::vector<int>::size_type>(mySegments_size)){
		throw std::runtime_error(std::string("mySegments.size() size cannot be cast to 'int'"));
	}
    for(int i=0; i<mySegments_size; ++i){
    	double mind=1000001;
    	int cl=-1;
		
    	//search for the closest center to the Segment
        if(mySegments[i].isc==1) {
            for(int c=0; c<centers_size_int; ++c){
        		if(centers[c]==i) cl=c;
    	    }
        }
        else{
            for(int c=0; c<centers_size_int; ++c){
        		double d = dist( mySegments[i], mySegments[centers[c]]) ;//+ 0.00001*((double) rand() / (RAND_MAX));
    	    	if(d < mind) { mind=d; cl=c; }
    	      }
        }    
        //assagint to it (if it exist)
    	if(cl>-1){
    	double d = dist( mySegments[i] , mySegments[ centers[cl] ]) ;
    	//print output; d<dpar set ok=1, otherwise ok=0;
    	  int cent;
    	  if( d >= dpar ){cl=-1, cent=-1;} else { cent=centers[cl] ; ++incl;}
    	  outfile << 
	  thisquery << " " <<
    	  i << " " <<  
    	  cent << " " << 
    	  cl << " "  << 
    	  d << " " << 
    	  mySegments[i].sid << " " << 
    	  mySegments[i].sstart << " " << 
    	  mySegments[i].send << " " <<
    	  mySegments[i].qstart << " " << 
    	  mySegments[i].qend << " " << 
    	  mySegments[i].score<< " " <<
          mySegments[i].evalue << " " <<
          mySegments[i].isc << " " <<
          mySegments[i].rho << " " <<
          mySegments[i].delta << " " <<
    	  endl;
    	   
    	 
    	 
    	}
    	else {
    	cout << " \n\nERROR CUSTERIZING" << endl;
    	return 2;    	
    	}
    
    }
   
   outfile.close();

   //cout << "\n\n INCL=" << incl << " NDATA=" << ndata << " diff:" << ndata-incl << " ratio:" << double(incl)/ndata << endl; 

   //cout << endl;   

//////////////////////////////////////////////////////// TO HERE

	}

	} // END WHILE


   return 0;


}





