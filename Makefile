# -*- Makefile -*-
#
# GLOBAL MAKEFILE (it makes other makes, located in src and src's subfolders)
#
SHELL=/bin/sh

# make all
all:
	$(MAKE) -C ./src/auxfiles/
	$(MAKE) -C ./src/distIcl/
	$(MAKE) -C ./src
	# bash -c "chmod +x *.sh"
	# bash -c "chmod +x tests/*.sh"  

# make only the src/auxfiles directory
auxfiles:
	$(MAKE) -C ./src/auxfiles/

# clean all
clean:
	$(MAKE) clean  -C ./src/auxfiles/
	$(MAKE) clean  -C ./src/distIcl/
	$(MAKE) clean  -C ./src
	$(MAKE) clean  -C ./tests

.PHONY: clean all