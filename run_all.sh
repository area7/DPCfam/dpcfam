#!/bin/bash
# Example of a complete DPCfam pipeline
RUN_PATH=${0%/*}
SCRIPT_PATH=${RUN_PATH}/scripts
# [ "." == "$RUN_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}
echo $RUN_PATH

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# source config file -> $DATA_FOLDER, $NUM_BLOCKS, $INPUT_FASTA
# set -a
source ${RUN_PATH}/configure
# set +a

# check env. variables
source ${SCRIPT_PATH}/aux/check_variables.sh

# keep track of errors
bad=0

###################################################
## Prepare input data
echo ">>> Step 0: prepare data"

${SCRIPT_PATH}/prepare_dataset.sh -o ${DATA_FOLDER} -n $NUM_BLOCKS $INPUT_FASTA

[ $? -ne 0 ] && die "Step 0 finished with errors" || printf ">>> Step 0: done!\n"

###################################################
## MAIN Step 1: run all-vs-all local pairwise alignment generation
## This runs blast_command.sh, and it will generate the blast db, and then run the blast alignments.
echo ">>> Step 1: blast alignments"

${SCRIPT_PATH}/blast_command.sh -o ${DATA_FOLDER} -n $NUM_BLOCKS ${DATA_FOLDER}/fastas

[ $? -ne 0 ] && die "Step 1 finished with errors" || printf ">>> Step 1: done!\n"

###################################################
## MAIN Step 2: run primary clustering
## Computes primary clusters
echo ">>> Step 2: primary clustering"

${SCRIPT_PATH}/primarycl_command.sh -o ${DATA_FOLDER}/primary_clusters -n $NUM_BLOCKS ${DATA_FOLDER}/alignments

[ $? -ne 0 ] && die "Step 2: finished with errors" || printf ">>> Step 2: done!\n"

###################################################
## MAIN Step 3: Compute secondary clustering / metaclustering
echo ">>> Step 3: secondary clustering"

###################################################
## Step 3.1: fix output file with only the columns needed to be transformed into binary, also remove non-clustered alignments
## Columns: query_ID cluster_ID search_ID search_START_position search_END_position
## (!) cluster_ID is not absolute id, it is query-specific
## Note that the file is sorted w.r.t. query_ID
echo "> Step 3.1: clean primary output file"

${SCRIPT_PATH}/clean_output_primary.sh -o ${DATA_FOLDER}/primary_clusters -n $NUM_BLOCKS ${DATA_FOLDER}/primary_clusters

[ $? -ne 0 ] && die "Step 3.1 finished with errors" || printf "> Step 3.1: done!\n"

###################################################
## Step 3.2: Transform all blocks to Binary
echo "> Step 3.2: convert to binary"

${SCRIPT_PATH}/generate_binary.sh -o ${DATA_FOLDER}/primary_clusters -n $NUM_BLOCKS ${DATA_FOLDER}/primary_clusters

[ $? -ne 0 ] && die "Step 3.2 finished with errors" || printf "> Step 3.2: done!\n"

###################################################
## MAIN Step 3.3 generate distance matrix between primary clusters
echo ">>> Step 3.3: generate distance matrix"

${SCRIPT_PATH}/compute_dmat.sh -o ${DATA_FOLDER}/distance_matrix -n $NUM_BLOCKS ${DATA_FOLDER}/primary_clusters

[ $? -ne 0 ] && die "Step 3.3 finished with errors" || printf "> Step 3.3: done!\n"

###################################################
## MAIN Step 3.4 METACLUSTERING + MERGING
echo ">>> Step 3.4: metaclustering and merging"

${SCRIPT_PATH}/metaclustering_command.sh -o ${DATA_FOLDER}/metaclusters -n $NUM_BLOCKS ${DATA_FOLDER}/distance_matrix

[ $? -ne 0 ] && die "Step 3.4 finished with errors" || printf "> Step 3.4: done!\n"

###################################################
## MAIN Step 4: traceback
echo ">>> Step 4: traceback"

${SCRIPT_PATH}/traceback_command.sh -o ${DATA_FOLDER}/metaclusters -n $NUM_BLOCKS -l ${DATA_FOLDER}/metaclusters/MCoutput.txt ${DATA_FOLDER}/primary_clusters

[ $? -ne 0 ] && die "Step 4 finished with errors" || printf ">>> Step 4: done!\n"

###################################################
## MAIN Step 5: filtering
echo ">>> Step 5: filtering"

${SCRIPT_PATH}/filtering_command.sh -o ${DATA_FOLDER}/metaclusters ${DATA_FOLDER}/metaclusters 
[ $? -ne 0 ] && die "Step 5 finished with errors" || printf ">>> Step 5: done!\n"

###################################################
## MAIN Step 6: GENERATE FINAL METACLUSTERS SEEDS (NON ALIGNED) with data (size, avglen, avgstd)
echo ">>> Step 6: generate final metaclusters seeds"

fasta_name="$(basename -- $INPUT_FASTA)"
${SCRIPT_PATH}/create_seeds.sh -o ${DATA_FOLDER}/metaclusters -t ${DATA_FOLDER}/${fasta_name}.tab ${DATA_FOLDER}/metaclusters/filtered-labels_ALL.txt
[ $? -ne 0 ] && die "Step 6 finished with errors" || printf ">>> Step 6: done!\n"
